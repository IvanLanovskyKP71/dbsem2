import scrapy


def isNotEmptyString(str):
    return len(str) > 0


class UkraineIsSpider(scrapy.Spider):
    links = []
    name = "ukraineis"
    custom_settings = {
        'ITEM_PIPELINES': {
            'db_lab_1.pipelines.NewsXmlPipeline': 300,
        }
    }
    fields = {
        'img': '//img/@src',
        'text': '//*[not(self::script)]/text()',
        'link': '//a/@href'
    }
    start_urls = [
        'http://www.ukraine-is.com/'
    ]
    allowed_domains = [
        'ukraine-is.com'
    ]

    def parse(self, response):
        
        text = filter(isNotEmptyString,
                      map(lambda str: str.strip(),
                          [text.extract() for text in response.xpath(self.fields["text"])]))
        images = map(lambda url: ((response.url + url) if url.startswith('/') else url),
                     [img_url.extract() for img_url in response.xpath(self.fields["img"])])

        self.links.append(response.url)

        yield {
            'text': text,
            'images': images,
            'url': response.url
        }
        for link_url in response.xpath(self.fields['link']):
            #print("%s" %(link_url.extract()))
            yield response.follow(link_url.extract(), callback=self.parse)
    
    def task2(self):
        print("Список гіперпосилань: \n")
        i = 1
        for link in UkraineIsSpider.links:
            print(i, link, sep='. ', end ='\n')
            i += 1
