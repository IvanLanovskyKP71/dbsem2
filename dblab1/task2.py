from scrapy import cmdline
import os
import lxml.etree as ET


def crawl():
    # try:
    #     os.remove("results/instrument.xml")
    # except OSError:
    #     print("results/instrument.xml not found")
    cmdline.execute("scrapy crawl instrument -o results/instrument.xml -t xml".split())
    #-o results/instrument.xml -t xml

def xslt_parse():
    dom = ET.parse('results/instrument.xml')
    xslt = ET.parse('instrument.xslt')
    transform = ET.XSLT(xslt)
    newdom = transform(dom)
    with open('results/instrument.html', 'wb') as f:
        f.write(ET.tostring(newdom, pretty_print=True))

crawl()
xslt_parse()
