from .BaseScene import BaseScene
from .UserActionsScene import UserActionsScene
from src import constants, User, Message
from PyInquirer import ValidationError, Validator
from random import random
from .EmulationScene import EmulationScene

EMULATION = '👾 emulation'
EXIT = '❌ exit'


class LoginValidator(Validator):
    def validate(self, document):
        if not len(str(document.text).strip()):
            raise ValidationError(
                message='Login cannot be empty',
                cursor_position=len(document.text))  # Move cursor to end


class GreetingScene(BaseScene):
    def __init__(self, session, redis):
        super().__init__([
            {
                'type': 'list',
                'name': 'role',
                'message': 'What do you want to do?',
                'choices': [
                    EMULATION,
                    EXIT
                ]
            },
            {
                'type': 'input',
                'name': 'login',
                'message': 'Your login',
                'when': lambda answers: answers['role'] == USER,
                'validate': LoginValidator
            }
        ])
        self.redis = redis
        self.chat = session['chat']
        self.thread = None
        self.session = session

    

    def handle_message_created_as_worker(self, message):
        self.process_message_as_worker(Message.load(message['data'], self.redis))

    def process_message_as_worker(self, message):
        print('⚠️ receive msg %s, working...' % message.id)
        if random() > 0.7:
            print('❌ block msg %s' % message.id)
            self.chat.block_message('auto_worker', message)
        else:
            print('✅ approve msg %s' % message.id)
            self.chat.approve_message('auto_worker', message)

    def __del__(self):
        if self.thread:
            self.thread.stop()


    @staticmethod
    def handle_else_message_created(msg):
        print('💬 %s create a message %s' % (msg['channel'].split(':')[1], msg['data']))

    @staticmethod
    def handle_else_incoming_message(msg):
        print('💬 %s has an incoming message %s' % (msg['channel'].split(':')[1], msg['data']))

    @staticmethod
    def handle_else_message_approve(msg):
        print('💬 %s message %s was approved' % (msg['channel'].split(':')[1], msg['data']))

    @staticmethod
    def handle_else_message_block(msg):
        print('💬 %s message %s was blocked' % (msg['channel'].split(':')[1], msg['data']))

    @staticmethod
    def handle_message_created(msg):
        print('⭐️ Your message %s created' % msg['data'])

    @staticmethod
    def handle_incoming_message(msg):
        print('⭐️ You have a new incoming message: %s' % msg['data'])

    @staticmethod
    def handle_message_approve(msg):
        print('✅ Your message %s was approved' % msg['data'])

    @staticmethod
    def handle_message_block(msg):
        print('⛔️ Your message %s was blocked' % msg['data'])

    def enter(self):
        while True:
            if self.thread:
                self.thread.stop()
                self.thread = None
            answers = self.ask()
            if 'role' not in answers:
                continue
            if answers['role'] == EXIT:
                return
            if answers['role'] == EMULATION:
                EmulationScene(self.session, self.redis).enter()
